<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8" />
        <title>{{ config('app.name', 'ChaseFit') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="PRO LOGIQG LABS Management Portal" name="description" />
        <meta content="PRO LOGIQ LABS" name="author" />

        <!-- App favicon -->
        <link href="{{ asset('assets/images/favicon.ico') }}" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/metismenu.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">


    </head>
