        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}" defer></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}" defer></script>
        <script src="{{ asset('assets/js/metisMenu.min.js') }}" defer></script>
        <script src="{{ asset('assets/js/waves.min.js') }}" defer></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.min.js') }}" defer></script>

        <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}" defer></script>
        <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" defer></script>

        <script src="{{ asset('assets/plugins/moment/moment.js') }}" defer></script>
        <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}" defer></script>
        <script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>
        <script src="https://apexcharts.com/samples/assets/series1000.js"></script>
        <script src="https://apexcharts.com/samples/assets/ohlc.js') }}" defer></script>
        <script src="{{ asset('assets/pages/jquery.dashboard.init.js') }}" defer></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.js') }}" defer></script>
