<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});

Route::get('/forgot', function () {
    return view('auth.forgot');
});

Route::get('/profile', function () {
    return view('user.profile');
});

Route::get('/terms', function () {
    return view('site.terms');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Bids Control
Route::get('/bids', 'ManageBidsController@index')->name('bids');
Route::get('/bids/new', 'ManageBidsController@newBid')->name('newBid');

// Client Management
Route::get('/client/new', 'ManageCustomerController@index')->name('newClient');
Route::get('/client/update', 'ManageCustomerController@updateClient')->name('updateClient');

// Site & Management
Route::get('/admin', 'ManageSiteController@admin')->name('admin');

// Podcast Stats
Route::get('/podcast', 'PodcastController@index')->name('podcast');
Route::get('/podcast/stats', 'PodcastController@stats')->name('stats');
