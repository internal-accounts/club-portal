<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManageCustomerController extends Controller
{
    //
    public function index()
    {
        return view('client.new');
    }
    public function updateClient()
    {
        return view('client.update');
    }
}
