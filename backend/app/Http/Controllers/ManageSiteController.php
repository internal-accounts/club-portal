<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManageSiteController extends Controller
{
    //
    public function index()
    {
        return view('site.index');
    }
    public function admin()
    {
        return view('site.admin');
    }
}
