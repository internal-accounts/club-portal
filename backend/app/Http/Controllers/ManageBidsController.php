<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class ManageBidsController extends Controller
{
    //

    public function index()
    {
        //
        $bid_details = DB::select('select * from bid_details');
        $count_bids = DB::select('select * from bid_details');
        $count_entity = DB::select('select * from entity');

        return view('bids', ['userFullName' => 'PRO LOGIQ LABS', 'bid_details' => $bid_details, 'count_bids' => $count_bids, 'count_entity' => $count_entity, 'counter' => 1]);
    }

    public function newBid()
    {
        return view('bids.new');
    }
}
