<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class PodcastController extends Controller
{
    //
    public function index()
    {
        $podcasts = DB::select('select * from podcast_details');
        $count_bids = DB::select('select * from bid_details');
        $count_entity = DB::select('select * from entity');
        $podacast_owner = DB::select('Select customers.customer_name from customers, podcast_details where customers.customer_number = podcast_details.podcast_owner');

        return view('podcast.review', ['userFullName' => 'PRO LOGIQ LABS', 'podcasts' => $podcasts, 'count_bids' => $count_bids, 'count_entity' => $count_entity, 'counter' => 1, 'podacast_owner' => $podacast_owner]);
    }
    public function stats()
    {
        return view('podcast.stats');
    }
}
