<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_name');
            $table->string('customer_number', 8);
            $table->string('contact_email');
            $table->string('contact_number');
            $table->string('contact_address_1');
            $table->string('contact_address_2');
            $table->string('contact_city');
            $table->string('contact_province');
            $table->string('contact_country');
            $table->string('contact_postal');
            $table->string('contact_vat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('customers');
    }
}
