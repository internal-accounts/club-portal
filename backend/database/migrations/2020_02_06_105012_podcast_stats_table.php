<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PodcastStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('podcast_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('podcast_name');
            $table->char('podcast_owner', 8);
            $table->enum('platform_name', ['Apple Podcast', 'Spotify', 'Google Podcast', 'Site']);
            $table->string('report_month');
            $table->integer('listners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('podcast_stats');
    }
}
