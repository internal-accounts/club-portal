<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PodcastCountryReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('podcast_country', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('podcast_name');
            $table->char('podcast_owner', 8);
            $table->string('country_name');
            $table->string('report_month');
            $table->integer('total_listners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('podcast_country');
    }
}
