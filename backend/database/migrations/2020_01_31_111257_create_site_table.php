<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_name');
            $table->string('site_description');
            $table->string('mailer_email');
            $table->string('mailer_server');
            $table->string('mailer_username');
            $table->string('mailer_password');
            $table->string('mailer_port');
            $table->string('mailer_name');
            $table->string('company_address_1');
            $table->string('company_address_2')->nullable();
            $table->string('company_city');
            $table->string('company_province');
            $table->string('company_postal');
            $table->string('company_telephone')->nullable();
            $table->string('company_cellphone')->nullable();
            $table->string('company_fax')->nullable();
            $table->string('company_email')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site');
    }
}
