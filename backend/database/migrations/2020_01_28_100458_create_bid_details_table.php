<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_name');
            $table->string('entity_code');
            $table->string('bid_type');
            $table->date('bid_posted');
            $table->dateTime('bid_due');
            $table->date('bid_expires');
            $table->string('bid_status');
            $table->string('bid_feedback')->default('WAITING FEEDBACK');
            $table->string('bid_number')->unique();
            $table->timestamp('bid_created')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestampTz('bid_updated')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->string('bid_submission');
            $table->text('bid_submission_details')->nullable();
            $table->string('bid_category')->nullable();
            $table->string('bid_doc_attachment')->nullable();
            $table->string('bid_region');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_details');
    }
}
