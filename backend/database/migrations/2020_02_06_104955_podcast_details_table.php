<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PodcastDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('podcast_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('podcast_name');
            $table->text('podcast_url')->default('images/itunes_image.png');
            $table->text('podcast_artwork');
            $table->enum('primary_category', ['Music', 'News/Podcast', 'Radio'])->default('Music');
            $table->char('podcast_region', 3)->default('ZAR');
            $table->char('podcast_owner', 8);
            $table->longText('podcast_host');
            $table->boolean('applepodcast')->default(true);
            $table->boolean('spotify')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('podcast_details');
    }
}
